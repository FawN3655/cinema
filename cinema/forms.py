from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import UsersModel


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = UsersModel
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = UsersModel
        fields = ('email',)

