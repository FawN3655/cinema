from rest_framework.views import APIView
from django.middleware.csrf import get_token
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate
from django.contrib.auth import login, logout
from django.core.exceptions import ObjectDoesNotExist
from .serializer import UserSerializer
from .models import UsersModel, ProfileModel


class LogInView(APIView):
    """Авторизация пользователей"""
    permission_classes = []

    def post(self, request):
        """
        Проводит авторихацию пользователя на сервере
        :param request: username and password
        :return: статус авторизации, csrf_token and auth_token
        """
        _user = request.user
        if _user.is_authenticated:
            token, __ = Token.objects.get_or_create(user=_user)
            return Response({'ms': 'Вы уже авторизированы', 'error': '',
                             'data': {'csrf_token': get_token(request), 'auth_token': token.key}},
                            status=status.HTTP_200_OK)
        else:
            email = request.data.get('email')
            password = request.data.get('password')
            if email and password:
                user = authenticate(request, email=email, password=password)
                if user:
                    login(request, user)
                    request.user = user
                    token, __ = Token.objects.get_or_create(user=user)
                    return Response(
                        {'ms': 'Авторизация успешна', 'error': '',
                         'data': {'csrf_token': get_token(request), 'auth_token': token.key}},
                        status=status.HTTP_200_OK)
                else:
                    return Response({'ms': 'Ошибка', 'error': 'Не верный email или password', 'data': []},
                                    status=status.HTTP_403_FORBIDDEN)
            else:
                return Response(
                    {'ms': 'Ошибка', 'error': 'Все поля (email и password) обязательны для заполнения', 'data': []},
                    status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        """
        Делает logout пользователя и удаляет токен
        :param request: None
        :return: Статус операции
        """
        try:
            request.auth.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass
        logout(request)
        return Response({'ms': 'Вы успешно вышли из системы', 'error': '', 'data': []}, status=status.HTTP_200_OK)

    def put(self, request):
        """
        обновляет токен переданый для авторизации
        :param request: None
        :return: Статус, csrf_token and auth_token
        """
        try:
            user = request.auth.user
            request.auth.delete()
            new_token = Token.objects.create(user=user)
        except Exception as e:
            return Response({'ms': 'Ошибка при обновление токена', 'error': '', 'data': [str(e)]},
                            status=status.HTTP_200_OK)
        return Response({'ms': 'Токен успешно обновлен', 'error': '',
                         'data': {'csrf_token': get_token(request), 'auth_token': new_token.key}},
                        status=status.HTTP_200_OK)


class SignUpView(APIView):
    """Создание новых пользователей"""
    permission_classes = []

    def post(self, request):
        """
        Создание нового пользователя
        :param request: email and password
        :return: csrf_token and auth_token
        """
        serialized = UserSerializer(data=request.data)
        if serialized.is_valid():
            user = UsersModel.objects.create_user(
                serialized.initial_data['email'],
                serialized.initial_data['password'],
                is_staff=serialized.initial_data['is_staff']  # Добавлено для простоты запуска файла examples.py
            )
            ProfileModel(user=user, role="client").save()
            token, __ = Token.objects.get_or_create(user=user)
            return Response({'ms': 'Аккаунт успешно создан', 'error': '',
                             'data': {'csrf_token': get_token(request), 'auth_token': token.key}},
                            status=status.HTTP_201_CREATED)
        else:
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)
