from django.db import models
from django.utils.translation import ugettext_lazy
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from .managers import UserManager


class UsersModel(AbstractBaseUser, PermissionsMixin):
    """Кастомная модель пользователей, для добавления пользователю новых полей (Так лучше не делать но можно,
    более правильным, по моему мнению, ьудит решение приведенное в примере ProfileModel)"""
    email = models.EmailField(ugettext_lazy('email address'), unique=True)
    role_id = models.IntegerField(blank=False, null=False, default=1)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email


class ProfileModel(models.Model):
    """Связь модели пользователей с другой таблтцей (сделано для примера и не используется)"""
    user = models.OneToOneField(UsersModel, on_delete=models.CASCADE, related_name="profile")
    role = models.CharField(max_length=800, null=False, blank=False)


class FilmsModel(models.Model):
    """Модель показывемых фльмов"""
    name = models.CharField(max_length=800, null=False, blank=False)
    poster = models.ImageField(upload_to="posters", height_field=None, width_field=None)
    time = models.DateTimeField(null=False, blank=False)
    price = models.IntegerField(null=False, blank=False)
    creator = models.ManyToManyField(UsersModel, related_name="creator")
    create_time = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.name


class PlacesModel(models.Model):
    """Модель доступных места на показ"""
    place_number = models.IntegerField(null=False, blank=False)
    is_free = models.BooleanField(default=False, null=False, blank=False)
    user = models.ForeignKey(UsersModel, related_name="user", blank=True, null=True, on_delete=models.CASCADE)
    film = models.ForeignKey(FilmsModel, related_name="places", on_delete=models.CASCADE)

    def __str__(self):
        return "Фильм {} в {}, место №{}".format(self.film.name, self.film.time.strftime("%m/%d/%Y %H:%M"), self.place_number)
