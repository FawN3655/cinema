from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import PlacesModel, FilmsModel
from .serializer import PlacesSerializer, FilmsAllSerializer, FilmsSerializer, LimitedPlacesSerializer
from django.core.paginator import Paginator
from datetime import datetime, timezone


def get_object(model, pk):
    """Получение обьекта"""
    try:
        return model.objects.get(pk=pk)
    except model.DoesNotExist:
        return None


def _is_staff(user):
    """Проверка уровня доступа"""
    if user.is_staff or user.role_id != 1 or user.profile.role == "sale":
        return True
    return False


def is_staff_decorator(func):
    """Декоратор для проверки уровня доступа"""

    def _wrapped_view(class_, request, *args, **kwargs):
        if _is_staff(request.user):
            return func(class_, request, *args, **kwargs)
        return Response({'ms': 'Error', 'error': 'У вас недостаточно прав для этого действия', 'data': []},
                        status=status.HTTP_403_FORBIDDEN)

    return _wrapped_view


class FilmsView(APIView):

    def get(self, request):
        """
        Получения списка сеансов с пагинацией
        :param request: page - нормер страницы
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные пагинатора
        data.page_count - доступное количество страниц
        data.current_page - текущаяя страница
        data.data - список с сенсами
        data.data.0.id - id сеанса
        data.data.0.name - названия сеанса
        data.data.0.poster - файл постера
        data.data.0.time - время сеанса
        data.data.0.price - цена билета
        data.data.0.create_time - время создания
        data.data.0.creator - создатель сеанса
        data.data.0.creator.id - id создателя
        data.data.0.creator.email - email создателя
        data.data.0.creator.role_id - id роли создателя
        data.data.0.places - список мест на сеанс
        data.data.0.places.0.id - id места
        data.data.0.places.0.user - пользователь занявший место
        data.data.0.creator.user.id - id пользователя
        data.data.0.creator.user.email - email пользователя
        data.data.0.creator.user.role_id - id роли пользователя
        data.data.0.places.0.place_number - номер места
        data.data.0.places.0.is_free - статус места
        """
        page = request.GET.get('page', 1)
        try:
            page = int(page)
        except ValueError:
            page = 1
        __ = FilmsModel.objects.all()
        _user = request.user
        data = FilmsAllSerializer(__, many=True) if _is_staff(_user) else FilmsSerializer(__, many=True)
        paginator = Paginator(data.data, 10)
        return Response({'ms': 'Список сеансов успешно получен', 'error': '', 'data': {
            "page_count": paginator.num_pages, "current_page":
                page if page <= paginator.num_pages else paginator.num_pages,
            'data': paginator.get_page(page).object_list}}, status=status.HTTP_200_OK)

    @is_staff_decorator
    def post(self, request):
        """
        Создание нового севнса и мест под него
        :param request: name - названия сеанса, time - время сеанса, price - цена билета,
        place_count - количество мест на сеансе
        :files poster - файл постера сеанса
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные созданного сеанса
        data.id - id сеанса
        data.name - названия сеанса
        data.poster - файл постера
        data.time - время сеанса
        data.price - цена билета
        data.create_time - время создания
        data.creator - создатель сеанса
        """
        place_count = request.data.get("place_count")
        if not place_count:
            return Response({'ms': 'Error', 'error': "place_count обязательное поле", 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            place_count = int(place_count)
        except ValueError:
            return Response({'ms': 'Error', 'error': "place_count должно быть числом", 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)
        if not place_count:
            return Response({'ms': 'Error', 'error': "hall обязательное поле", 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)
        time = request.data.get("time")
        if not place_count:
            return Response({'ms': 'Error', 'error': "time обязательное поле", 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = FilmsAllSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(creator=(request.user,))
            place_data = [dict(place_number=i + 1, is_free=True, time=time) for i in range(place_count)]
            place_serializer = PlacesSerializer(data=place_data, many=True)
            if place_serializer.is_valid():
                place_serializer.save(film=FilmsModel.objects.filter(id=serializer.data.get("id")).first())
                return Response({'ms': 'Сеанс успешно создан', 'error': "", 'data': serializer.data},
                                status=status.HTTP_201_CREATED)
            else:
                return Response({'ms': 'Error', 'error': place_serializer.errors, 'data': []},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'ms': 'Error', 'error': serializer.errors, 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)


class FilmView(APIView):

    def get(self, request, pk):
        """
        Получение данных конкретного сеанса
        :param request: None
        :param pk: id сеанса
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные созданного сеанса
        data.id - id сеанса
        data.name - названия сеанса
        data.poster - файл постера
        data.time - время сеанса
        data.price - цена билета
        data.create_time - время создания
        data.creator - создатель сеанса
        """
        film = get_object(FilmsModel, pk)
        if not film:
            return Response({'ms': 'Error', 'error': "Сеанс с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        _user = request.user
        data = FilmsAllSerializer(film) if _is_staff(_user) else FilmsSerializer(film)
        return Response({'ms': 'Данные сеанса успешно получены', 'error': "", 'data': data.data},
                        status=status.HTTP_200_OK)

    @is_staff_decorator
    def delete(self, request, pk):
        """
        Удаление конкретного сеанса и мест под него, удаление не возможно если есть приобретенные места и
        сейанс еще не происходил
        :param request: None
        :param pk: id сеанса
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные удаленного сеанса
        data.id - id сеанса
        data.name - названия сеанса
        data.poster - файл постера
        data.time - время сеанса
        data.price - цена билета
        data.create_time - время создания
        data.creator - создатель сеанса
        """
        film = get_object(FilmsModel, pk)
        if not film:
            return Response({'ms': 'Error', 'error': "Сеанс с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        if film.time > datetime.now(timezone.utc) and film.places.filter(is_free=False):
            return Response({'ms': 'Error', 'error': "Удаление данного сеанса невозможно, так как показ еще не "
                                                     "состоялся и на него есть приобретенные места", 'data': []},
                            status=status.HTTP_403_FORBIDDEN)
        deleted_data = FilmsAllSerializer(film).data
        film.delete()
        return Response({'ms': 'Данные сеанса успешно удалены', 'error': "", 'data': deleted_data},
                        status=status.HTTP_200_OK)

    @is_staff_decorator
    def put(self, request, pk):
        """
        Редактирование определенного сеанса
        :param request: None
        :param pk: id сеанса
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные имененного сеанса
        data.id - id сеанса
        data.name - названия сеанса
        data.poster - файл постера
        data.time - время сеанса
        data.price - цена билета
        data.create_time - время создания
        data.creator - создатель сеанса
        """
        film = get_object(FilmsModel, pk)
        if not film:
            return Response({'ms': 'Error', 'error': "Сеанс с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        serializer = FilmsAllSerializer(film, data=request.data)
        if serializer.is_valid():
            serializer.save(creator=(request.user,))
            return Response({'ms': 'Сеанс успешно изменен', 'error': "", 'data': serializer.data},
                            status=status.HTTP_200_OK)
        return Response({'ms': 'Error', 'error': serializer.errors, 'data': []},
                        status=status.HTTP_400_BAD_REQUEST)


class PlacesView(APIView):

    def get(self, request):
        """
        Получение списка фильмов по id фильма
        :param request: film_id - id фильма к которому нужно получить места
        :return: список мест для фильма, в зависимости от пользователя
        """
        _user = request.user
        film_id = request.GET.get("film_id")
        if not film_id:
            return Response({'ms': 'Error', 'error': "film_id обязательное поле", 'data': []},
                            status=status.HTTP_400_BAD_REQUEST)
        film = get_object(FilmsModel, film_id)
        if not film:
            return Response({'ms': 'Error', 'error': "Сеанс с id {} не найден".format(film_id), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        __ = PlacesModel.objects.filter(film=film)
        data = PlacesSerializer(__, many=True) if _is_staff(_user) else LimitedPlacesSerializer(__, many=True)
        return Response({'ms': 'Данные мест для сеанса сеанса успешно получены', 'error': "", 'data': data.data},
                        status=status.HTTP_200_OK)


class PlaceView(APIView):

    def get(self, request, pk):
        """
        Получение данных конкретного места на сеансе
        :param request: None
        :param pk: id места
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные места
        id - id места
        user - пользователь занявший место (если есть)
        place_number - номер места
        is_free - статус места
        """
        place = get_object(PlacesModel, pk)
        if not place:
            return Response({'ms': 'Error', 'error': "Место с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        data = PlacesSerializer(place) if _is_staff(request.user) else LimitedPlacesSerializer(place)
        return Response({'ms': 'Данные сеанса успешно получены', 'error': "", 'data': data.data},
                        status=status.HTTP_200_OK)

    @is_staff_decorator
    def delete(self, request, pk):
        """
        Удаление конкретного места, доступно только сотрудникам
        Удалить занятое место нельзя
        :param request: None
        :param pk: id места
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные удаленного места
         id - id места
        user - пользователь занявший место (если есть)
        place_number - номер места
        is_free - статус места
        """
        place = get_object(PlacesModel, pk)
        if not place:
            return Response({'ms': 'Error', 'error': "Место с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        if place.is_free is False:
            return Response({'ms': 'Error', 'error': "Данное место уже занято, удалить его можно только вместе с "
                                                     "сеансом", 'data': []}, status=status.HTTP_404_NOT_FOUND)
        deleted_data = PlacesSerializer(place).data
        place.delete()
        return Response({'ms': 'Данные места для сеанса успешно удалены', 'error': "", 'data': deleted_data},
                        status=status.HTTP_200_OK)

    @is_staff_decorator
    def put(self, request, pk):
        """
        Редактирование места
        :param request: place_number - номер места, is_free - статус места, hall - имя или номер зала
        :param pk: id места
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные купленного места
        user - пользователь занявший место (если есть)
        place_number - номер места
        is_free - статус места
        hall - имя или номер зала
        time - время сеанса
        """
        place = get_object(PlacesModel, pk)
        if not place:
            return Response({'ms': 'Error', 'error': "Место с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        serializer = PlacesSerializer(place, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'ms': 'Место успешно изменено', 'error': "", 'data': serializer.data},
                            status=status.HTTP_200_OK)
        return Response({'ms': 'Error', 'error': serializer.errors, 'data': []},
                        status=status.HTTP_400_BAD_REQUEST)


class TicketsView(APIView):

    def post(self, request, pk):
        """
        Покупка билета
        :param request: None
        :param pk: id места
        :return:  ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные купленного места
         id - id места
        user - пользователь занявший место (если есть)
        place_number - номер места
        is_free - статус места
        hall - имя или номер зала
        time - время сеанса
        """
        place = get_object(PlacesModel, pk)
        if not place:
            return Response({'ms': 'Error', 'error': "Место с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        if place.is_free is False:
            return Response({'ms': 'Error', 'error': "Данное место уже занято", 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        # Оплату добавлять здесь
        place.is_free = False
        place.user = request.user
        place.save()
        return Response({'ms': 'Место успешно приобретено', 'error': "", 'data': PlacesSerializer(place).data},
                        status=status.HTTP_200_OK)

    def delete(self, request, pk):
        """
        Отмена покупки билета
        :param request: None
        :param pk: id места
        :return: ms - сообщение о статусе операции, error - текст ошибки если есть, data - данные купленного места
         id - id места
        user - пользователь занявший место (если есть)
        place_number - номер места
        is_free - статус места
        hall - имя или номер зала
        time - время сеанса
        """
        place = get_object(PlacesModel, pk)
        if not place:
            return Response({'ms': 'Error', 'error': "Место с id {} не найден".format(pk), 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        if place.is_free is True:
            return Response({'ms': 'Error', 'error': "Данное место не занято", 'data': []},
                            status=status.HTTP_404_NOT_FOUND)
        # Refund добавлять здесь
        place.is_free = True
        place.user = None
        place.save()
        return Response({'ms': 'Покупка успешно отменена', 'error': "", 'data': PlacesSerializer(place).data},
                        status=status.HTTP_200_OK)
