from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import UsersModel, ProfileModel, PlacesModel, FilmsModel


class AreaInline(admin.StackedInline):
    model = ProfileModel


class CustomUserAdmin(UserAdmin):
    inlines = (AreaInline,)
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = UsersModel
    list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active', "role_id")}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active', "role_id")}
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(UsersModel, CustomUserAdmin)
admin.site.register(PlacesModel)
admin.site.register(FilmsModel)
