"""cinema URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .views_auth import LogInView, SignUpView
from django.conf.urls.static import static
from django.conf import settings
from .views_film import FilmsView, FilmView, PlaceView, TicketsView, PlacesView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', LogInView.as_view()),
    path('signup/', SignUpView.as_view()),
    path('films/', FilmsView.as_view()),
    path('places/', PlacesView.as_view()),
    url(r'^film/(?P<pk>[0-9]+)/$', FilmView.as_view()),
    url(r'^places/(?P<pk>[0-9]+)/$', PlaceView.as_view()),
    url(r'^ticket/(?P<pk>[0-9]+)/$', TicketsView.as_view()),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
