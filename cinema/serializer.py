from rest_framework import serializers
from .models import ProfileModel, UsersModel, PlacesModel, FilmsModel


class UserProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = ProfileModel
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersModel
        fields = '__all__'


class PublicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersModel
        fields = ("id", "email", "role_id", "is_staff", "is_active", "groups", "user_permissions",)


class FilmsAllSerializer(serializers.ModelSerializer):
    creator = PublicUserSerializer(read_only=True, many=True)

    class Meta:
        model = FilmsModel
        fields = '__all__'


class FilmsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FilmsModel
        fields = ("id", "name", "poster", "time", "price",)


class LimitedPlacesSerializer(serializers.ModelSerializer):
    film = FilmsSerializer(read_only=True)

    class Meta:
        model = PlacesModel
        fields = ("id", "is_free", "film",)


class PlacesSerializer(serializers.ModelSerializer):
    user = PublicUserSerializer(read_only=True)
    film = FilmsAllSerializer(read_only=True)

    class Meta:
        model = PlacesModel
        fields = '__all__'
