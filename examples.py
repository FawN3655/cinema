import requests
import random
from os.path import join, dirname
from datetime import datetime, timezone, timedelta

# url = "http://127.0.0.1:8000"
url = "https://hidden-ravine-28204.herokuapp.com"
email = "email{}@gmail.com".format(random.randint(1, 1000000))
password = "123"

session = requests.Session()

# signup
response = session.post("{}/signup/".format(url), json={"email": email, "password": password, "is_staff": True})
print(response.status_code, response.json())

# Login
response = session.post("{}/login/".format(url), json={"email": email, "password": password})
session.headers['Authorization'] = 'token {}'.format(response.json().get("data", {}).get('auth_token'))
print(response.status_code, response.json())

response = session.put("{}/login/".format(url))
session.headers['Authorization'] = 'token {}'.format(response.json().get("data", {}).get('auth_token'))
print(response.status_code, response.json())

response = session.delete("{}/login/".format(url))
del session.headers["Authorization"]
print(response.status_code, response.json())

response = session.post("{}/login/".format(url), json={"email": email, "password": password})
session.headers['Authorization'] = 'token {}'.format(response.json().get("data", {}).get('auth_token'))
print(response.status_code, response.json())

file = open(join(dirname(__file__), "static/test.png"), "rb")
response = session.post("{}/films/".format(url),
                        data={"name": "name", "time": datetime.now(), "price": 10, "place_count": 10, "hall": "2 Godzilla"},
                        files={"poster": file})
file.close()
film_id = response.json().get("data", {}).get("id")
print(response.status_code, response.json())

response = session.get("{}/films/".format(url))
print(response.status_code, response.json())

response = session.get("{}/film/{}/".format(url, film_id))
print(response.status_code, response.json())

file = open(join(dirname(__file__), "static/test2.png"), "rb")
response = session.put("{}/film/{}/".format(url, film_id),
                       data={"name": "test1", "time": datetime.now(timezone.utc) + timedelta(hours=1), "price": 5},
                       files={"poster": file})
file.close()
print(response.status_code, response.json())

response = session.get("{}/places/?film_id={}".format(url, film_id))
print(response.status_code, response.json())
place_id = response.json().get("data", {})[0].get("id")


response = session.get("{}/places/{}/".format(url, place_id))
print(response.status_code, response.json())

response = session.post("{}/ticket/{}/".format(url, place_id))
print(response.status_code, response.json())

response = session.delete("{}/ticket/{}/".format(url, place_id))
print(response.status_code, response.json())

response = session.put("{}/places/{}/".format(url, place_id), data={"place_number": 12, "is_free": True, "hall": "2 Zorro"})
print(response.status_code, response.json())

response = session.delete("{}/places/{}/".format(url, place_id))
print(response.status_code, response.json())

response = session.delete("{}/film/{}/".format(url, film_id))
print(response.status_code, response.json())
